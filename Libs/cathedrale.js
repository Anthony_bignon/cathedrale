function Cathedrale() {
    var container; 
            
    var camera, scene, renderer, player;
    var dax=0.002;

    function onWindowResize()
    {
        camera.aspect = window.innerWidth / window.innerHeight;
        camera.updateProjectionMatrix();
        renderer.setSize( window.innerWidth -10  , window.innerHeight-20);
    }

    function Init()
    {
        container = document.createElement( 'div' );
        document.body.appendChild( container );

        camera = new THREE.PerspectiveCamera( 90, window.innerWidth / window.innerHeight, 1, 1000 );
        camera.rotation.z=0;
        camera.rotation.x=Math.PI/2;
        camera.rotation.y=Math.PI;
        camera.position.z=-20;

        // scene
        scene = new THREE.Scene();
        
        var ambient = new THREE.AmbientLight( 0x160D08 );
        scene.add( ambient );

        light = new THREE.PointLight( 0xffffff, 1, 8);
        light.position.set(camera.position.x, camera.position.y, camera.position.z );
        scene.add(light);
      

        // texture

        var manager = new THREE.LoadingManager();
        manager.onProgress = function ( item, loaded, total )
        {
            console.log( item, loaded, total );
        };

        var texture = new THREE.Texture();
        

        var loader = new THREE.ImageLoader( manager );
        loader.load( 'TranseptSud/TranseptTexture4096.jpg', function ( image )
        {
            texture.image = image;
            texture.needsUpdate = true;
        } );

        // Chargement du modèle
        var loader = new THREE.OBJLoader( manager );
        loader.load( 'TranseptSud/transeptSudBox.obj', function ( object )
        {
            object.traverse( function ( child )
            {
                if ( child instanceof THREE.Mesh )
                {
                    child.material.map = texture;
                }
            } );
            scene.add( object );
        } );
        renderer = new THREE.WebGLRenderer();
        renderer.setSize( window.innerWidth-10, window.innerHeight-20);
        console.log("width = " + window.innerWidth + " height = " + window.innerHeight)
        container.appendChild( renderer.domElement );
        window.addEventListener( 'resize', onWindowResize, false );
    }

    function Afficher()
    {
        renderer.render(scene,camera);
    }

    function Animer()
    {
        requestAnimationFrame(Animer);
        Afficher();
    }

    var movement = 0.20;
    var updown = Math.PI/32;
    var maxLargeur = 4.46;
    var maxLongueur = 9.80;

    Init();
    Animer();
    window.addEventListener("keydown", function(event) {
        inputClavier(event);
    } ,true);


    function inputClavier(event) {
        if (!event.defaultPrevented)
        {
            switch(event.which) {
                case 40:
                    if (event.shiftKey == true)
                        camera.rotation.x += updown;
                    else
                    {
                        camera.position.x -= Math.sin(-camera.rotation.y) * movement;
                        camera.position.y -= Math.cos(-camera.rotation.y) * movement;                  
                        verifXSortieCathedrale()
                        light.position.x = camera.position.x
                        light.position.y = camera.position.y
                    }
                    break;
                case 38:
                    if (event.shiftKey == true)
                        camera.rotation.x -= updown;
                    else
                    {
                        camera.position.x += Math.sin(-camera.rotation.y) * movement;
                        camera.position.y += Math.cos(-camera.rotation.y) * movement;         
                        verifXSortieCathedrale()
                        light.position.x = camera.position.x
                        light.position.y = camera.position.y
                    }
                    break;
                case 37:
                    if(event.altKey == true)
                    {
                        camera.position.x -= Math.cos(-camera.rotation.y) * movement;
                        camera.position.y -= Math.sin(-camera.rotation.y) * movement;
                        verifXSortieCathedrale()
                        light.position.x = camera.position.x
                        light.position.y = camera.position.y
                    }
                    else
                        camera.rotation.y += updown;
                    break;
                case 39:
                    if (event.altKey == true)
                    {
                        camera.position.x += Math.cos(-camera.rotation.y) * movement;
                        camera.position.y += Math.sin(-camera.rotation.y) * movement;
                        verifXSortieCathedrale()
                        light.position.x = camera.position.x
                        light.position.y = camera.position.y
                    }
                    else
                        camera.rotation.y -= updown;
                    break;
            }
            Animer();
            event.preventDefault();   
        }   
    }
    function verifXSortieCathedrale() {
        if((camera.position.y > maxLongueur) && (camera.position.y > 9.9)){
            camera.position.y = maxLongueur - 0.10
        }
        else if((camera.position.y < (maxLongueur * -1)) && (camera.position.y < -9.9)){
            camera.position.y = (maxLongueur * -1) + 0.10
        }
        if((camera.position.x > maxLargeur) && (camera.position.x > maxLargeur -0.20)){
            camera.position.x = maxLargeur - 0.10
        }
        else if((camera.position.x < (maxLargeur * -1)) && (camera.position.x < (maxLargeur * -1) + 0.20)){
            camera.position.x = maxLargeur * -1 + 0.10
        }

    }
}
